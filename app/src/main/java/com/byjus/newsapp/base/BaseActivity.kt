package com.byjus.newsapp.base

import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity<V : BaseView, S : BaseState, P : BasePresenter<V, S>> :
    AppCompatActivity() {

    abstract val presenter: P
}