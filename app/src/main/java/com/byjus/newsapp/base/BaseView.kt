package com.byjus.newsapp.base

interface BaseView

interface BasePresenter<V : BaseView, S : BaseState> {
    var view: V?

    fun attachView(view: V) {
        this.view = view
    }

    fun detachView() {
        view = null
    }

    fun updateView(state: S)
}